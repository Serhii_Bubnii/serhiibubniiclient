package com.services.soap;

import com.epam.bubnii.service.soap.Category;
import com.epam.bubnii.service.soap.News;
import com.epam.bubnii.service.soap.NewsServiceSoap;
import com.epam.bubnii.service.soap.NewsServiceSoapImplService;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;


public class TestSOAP {

    private NewsServiceSoap newsServiceSoap;
    public static Logger LOGGER = Logger.getLogger(TestSOAP.class);

    @DataProvider
    public Iterator<Object[]> createNews() {
        LOGGER.info("Call data provider for create entity");
        return Stream.of(
                new Object[]{"Political news", "political description",
                        "body political news", "link to political news", "POLITICAL", 35},
                new Object[]{"Economic news", "economic description",
                        "body economic news", "link to economic news", "ECONOMIC", 36},
                new Object[]{"Sport news", "sport description",
                        "body sport news", "link to sport news", "SPORT", 37}
        ).iterator();
    }

    @DataProvider
    public Iterator<Object[]> updateNews() {
        LOGGER.info("Call data provider for update entity");
        return Stream.of(
                new Object[]{32, "Political news", "political description",
                        "body political news", "link to political news", "ECONOMIC"},
                new Object[]{33, "Economic news", "economic description",
                        "body economic news", "link to economic news", "SPORT"},
                new Object[]{34, "Sport news", "sport description",
                        "body sport news", "link to sport news", "POLITICAL"}
        ).iterator();
    }

    @BeforeClass
    public void setUp() {
        LOGGER.info("Set up configuration soap service");
        newsServiceSoap = new NewsServiceSoapImplService().getNewsServiceSoapImplPort();
    }

    @Test(dataProvider = "createNews")
    public void testCreateNewsUseSoapService(
            String title, String description, String bodyNews,
            String linkToNews, String category, Integer candidateId) {
        LOGGER.info("Test create news with parameters" + title + " " + category + " " +
                description + "\n" + linkToNews + "\n" + bodyNews);
        News news = new News();
        news.setTitle(title);
        news.setDescription(description);
        news.setBodyNews(bodyNews);
        news.setLinkToNews(linkToNews);
        news.setCategory(Category.valueOf(category));
        int beforeSizeBD = newsServiceSoap.findAll().size();
        LOGGER.info("Before size bd -- " + beforeSizeBD);
        assertEquals(candidateId, newsServiceSoap.create(news));
        int afterSizeBD = newsServiceSoap.findAll().size();
        LOGGER.info("After size bd -- " + afterSizeBD);
        assertNotEquals(beforeSizeBD, afterSizeBD);
    }


    @Test(dataProvider = "updateNews")
    public void testUpdateNewsUseSoapService(
            Integer id, String title, String description,
            String bodyNews, String linkToNews, String category) {
        LOGGER.info("Test update news with parameters" + id + " " + title + " " + category + " " +
                description + "\n" + linkToNews + "\n" + bodyNews);
        News news = new News();
        news.setId(id);
        news.setTitle(title);
        news.setDescription(description);
        news.setBodyNews(bodyNews);
        news.setLinkToNews(linkToNews);
        news.setCategory(Category.valueOf(category));
        newsServiceSoap.update(news);
    }

    @Test
    public void testGetAllNewsUseSoapService() {
        assertEquals(9, newsServiceSoap.findAll().size());
    }

    @Test
    public void testGetAllNewsByCategoryUseSoapService() {
        System.out.println(newsServiceSoap.findAllByCategory(Category.ECONOMIC));
        assertEquals(2, newsServiceSoap.findAllByCategory(Category.ECONOMIC).size());
    }

    @Test
    public void testGetNewsByCategoryUseSoapService() {
        Integer idNews = 33;
        LOGGER.info("find News by id = " + idNews + " = " + newsServiceSoap.findById(idNews));
        assertEquals(News.class, newsServiceSoap.findById(idNews).getClass());
    }
}
