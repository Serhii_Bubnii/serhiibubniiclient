package com.services.rest;

import com.epam.bubnii.utills.HelperMethods;
import com.epam.bubnii.utills.RestUtil;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static java.util.Objects.isNull;
import static org.testng.Assert.assertEquals;

public class TestRESTful {

    protected Response res = null; //Response object
    protected JsonPath jp = null; //JsonPath object
    public static Logger LOGGER = Logger.getLogger(TestRESTful.class);

    @BeforeClass
    public void setUpBaseParameters() {
        String baseURL = System.getProperty("server_host");
        if (isNull(baseURL)) {
            RestUtil.setBaseURL("http://localhost");
        } else {
            RestUtil.setBaseURL(baseURL);
        }
        String basePort = System.getProperty("server_port");
        if (isNull(basePort)) {
            RestUtil.setBasePort(9999);
        } else {
            RestUtil.setBasePort(Integer.parseInt(basePort));
        }
        String basePath = System.getProperty("server_base_path");
        if (isNull(basePath)) {
            RestUtil.setBasePath("/SerhiiBubniiService/");
        } else {
            RestUtil.setBasePath(basePath);
        }
//        RestUtil.createSearchQueryPath("news", "by-query", "id", "28"); //Construct the path
        RestUtil.createSearchPathParam("news/by", "POLITICAL");
        res = RestUtil.getResponse(); //Get response
        jp = RestUtil.getJsonPath(res); //Get JsonPath
    }

    @Test
    public void testNewsSearchByCategory() {
        Assert.assertEquals("Id does not match!", "POLITICAL",
                HelperMethods.getNewsCategoryList(jp).get(0).toString());
    }

    @Test
    public void createEmployee() {
        String requestBody = "{\n" +
                "\"title\": \"test\",\n" +
                "    \"description\": \"test\",\n" +
                "    \"bodyNews\": \"test\",\n" +
                "    \"linkToNews\": \"test\",\n" +
                "    \"category\": \"test\"\n" +
                "}";
        res = RestUtil.getResponse(); //Get response
        try {
            res = RestAssured.given()
                    .contentType(ContentType.JSON)
                    .body(requestBody)
                    .post("/add");
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info("Response :" + res.asString());
        LOGGER.info("Status Code :" + res.getStatusCode());
        LOGGER.info("Does Reponse contains 'tammy'? :" + res.asString().contains("tammy"));
        assertEquals(200, res.getStatusCode());
    }

    @Test
    public void testStatusCodeResponse() {
        HelperMethods.checkStatusIs200(res);
    }

    @Test
    public void testVerifyOnlyFourVideosReturned() {
        Assert.assertEquals(5,
                HelperMethods.getNewsById(jp).size(), "News Size is not equal to 1");
    }

    @AfterClass
    public void destroyBaseParameters() {
        RestUtil.resetBaseURL();
        RestUtil.resetBasePort();
        RestUtil.resetBasePath();
    }
}