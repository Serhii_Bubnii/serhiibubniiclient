
package com.epam.bubnii.service.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.bubnii.service.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindAll_QNAME = new QName("http://soap.service.bubnii.epam.com/", "findAll");
    private final static QName _FindAllByCategory_QNAME = new QName("http://soap.service.bubnii.epam.com/", "findAllByCategory");
    private final static QName _UpdateResponse_QNAME = new QName("http://soap.service.bubnii.epam.com/", "updateResponse");
    private final static QName _Create_QNAME = new QName("http://soap.service.bubnii.epam.com/", "create");
    private final static QName _CreateResponse_QNAME = new QName("http://soap.service.bubnii.epam.com/", "createResponse");
    private final static QName _Update_QNAME = new QName("http://soap.service.bubnii.epam.com/", "update");
    private final static QName _FindAllByCategoryResponse_QNAME = new QName("http://soap.service.bubnii.epam.com/", "findAllByCategoryResponse");
    private final static QName _FindAllResponse_QNAME = new QName("http://soap.service.bubnii.epam.com/", "findAllResponse");
    private final static QName _FindById_QNAME = new QName("http://soap.service.bubnii.epam.com/", "findById");
    private final static QName _FindByIdResponse_QNAME = new QName("http://soap.service.bubnii.epam.com/", "findByIdResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.bubnii.service.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindById }
     * 
     */
    public FindById createFindById() {
        return new FindById();
    }

    /**
     * Create an instance of {@link FindByIdResponse }
     * 
     */
    public FindByIdResponse createFindByIdResponse() {
        return new FindByIdResponse();
    }

    /**
     * Create an instance of {@link Create }
     * 
     */
    public Create createCreate() {
        return new Create();
    }

    /**
     * Create an instance of {@link CreateResponse }
     * 
     */
    public CreateResponse createCreateResponse() {
        return new CreateResponse();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link FindAllByCategoryResponse }
     * 
     */
    public FindAllByCategoryResponse createFindAllByCategoryResponse() {
        return new FindAllByCategoryResponse();
    }

    /**
     * Create an instance of {@link FindAllResponse }
     * 
     */
    public FindAllResponse createFindAllResponse() {
        return new FindAllResponse();
    }

    /**
     * Create an instance of {@link FindAll }
     * 
     */
    public FindAll createFindAll() {
        return new FindAll();
    }

    /**
     * Create an instance of {@link FindAllByCategory }
     * 
     */
    public FindAllByCategory createFindAllByCategory() {
        return new FindAllByCategory();
    }

    /**
     * Create an instance of {@link UpdateResponse }
     * 
     */
    public UpdateResponse createUpdateResponse() {
        return new UpdateResponse();
    }

    /**
     * Create an instance of {@link News }
     * 
     */
    public News createNews() {
        return new News();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "findAll")
    public JAXBElement<FindAll> createFindAll(FindAll value) {
        return new JAXBElement<FindAll>(_FindAll_QNAME, FindAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByCategory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "findAllByCategory")
    public JAXBElement<FindAllByCategory> createFindAllByCategory(FindAllByCategory value) {
        return new JAXBElement<FindAllByCategory>(_FindAllByCategory_QNAME, FindAllByCategory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "updateResponse")
    public JAXBElement<UpdateResponse> createUpdateResponse(UpdateResponse value) {
        return new JAXBElement<UpdateResponse>(_UpdateResponse_QNAME, UpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Create }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "create")
    public JAXBElement<Create> createCreate(Create value) {
        return new JAXBElement<Create>(_Create_QNAME, Create.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "createResponse")
    public JAXBElement<CreateResponse> createCreateResponse(CreateResponse value) {
        return new JAXBElement<CreateResponse>(_CreateResponse_QNAME, CreateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Update }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "update")
    public JAXBElement<Update> createUpdate(Update value) {
        return new JAXBElement<Update>(_Update_QNAME, Update.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByCategoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "findAllByCategoryResponse")
    public JAXBElement<FindAllByCategoryResponse> createFindAllByCategoryResponse(FindAllByCategoryResponse value) {
        return new JAXBElement<FindAllByCategoryResponse>(_FindAllByCategoryResponse_QNAME, FindAllByCategoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "findAllResponse")
    public JAXBElement<FindAllResponse> createFindAllResponse(FindAllResponse value) {
        return new JAXBElement<FindAllResponse>(_FindAllResponse_QNAME, FindAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "findById")
    public JAXBElement<FindById> createFindById(FindById value) {
        return new JAXBElement<FindById>(_FindById_QNAME, FindById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.bubnii.epam.com/", name = "findByIdResponse")
    public JAXBElement<FindByIdResponse> createFindByIdResponse(FindByIdResponse value) {
        return new JAXBElement<FindByIdResponse>(_FindByIdResponse_QNAME, FindByIdResponse.class, null, value);
    }

}
