
package com.epam.bubnii.service.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for news complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="news">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bodyNews" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="category" type="{http://soap.service.bubnii.epam.com/}category" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="linkToNews" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "news", propOrder = {
        "bodyNews",
        "category",
        "description",
        "id",
        "linkToNews",
        "title"
})
public class News {

    protected String bodyNews;
    @XmlSchemaType(name = "string")
    protected Category category;
    protected String description;
    protected Integer id;
    protected String linkToNews;
    protected String title;

    /**
     * Gets the value of the bodyNews property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBodyNews() {
        return bodyNews;
    }

    /**
     * Sets the value of the bodyNews property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBodyNews(String value) {
        this.bodyNews = value;
    }

    /**
     * Gets the value of the category property.
     *
     * @return possible object is
     * {@link Category }
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     *
     * @param value allowed object is
     *              {@link Category }
     */
    public void setCategory(Category value) {
        this.category = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the linkToNews property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLinkToNews() {
        return linkToNews;
    }

    /**
     * Sets the value of the linkToNews property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLinkToNews(String value) {
        this.linkToNews = value;
    }

    /**
     * Gets the value of the title property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    @Override
    public String toString() {
        return "News{" +
                " id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", bodyNews='" + bodyNews + '\'' +
                ", category=" + category +
                ", linkToNews='" + linkToNews + '\'' +
                '}';
    }
}
