package com.epam.bubnii.service.rest;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestClient {

    private URL url;
    private HttpURLConnection connection;
    public static Logger LOGGER = Logger.getLogger(RestClient.class);

    public void findAll() throws IOException {
        LOGGER.info("Find all news user RESTfull");
        url = new URL("http://localhost:9999/SerhiiBubniiService/news/all");
        LOGGER.info("Open connection");
        connection = (HttpURLConnection) url.openConnection();
        LOGGER.info("Set request parameters");
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "MediaType.APPLICATION_JSON");
        LOGGER.info("Check if the response status code is OK, Response Code -- " + connection.getResponseCode());
        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connection.getResponseCode());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String out;
        LOGGER.info("Output from Server .... \n");
        while ((out = reader.readLine()) != null) {
            LOGGER.info(out);
        }
        connection.disconnect();
    }

    public void findAllByCategory(String category) throws IOException {
        LOGGER.info("Find all news by category user RESTfull");
        String categoryUpperCase = category.toUpperCase();
        url = new URL("http://localhost:9999/SerhiiBubniiService/news/by/" + categoryUpperCase);
        LOGGER.info("Open connection");
        connection = (HttpURLConnection) url.openConnection();
        LOGGER.info("Set request parameters");
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "MediaType.APPLICATION_JSON");
        LOGGER.info("Check if the response status code is OK, Response Code -- " + connection.getResponseCode());
        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connection.getResponseCode());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String out;
        LOGGER.info("Output from Server .... \n");
        while ((out = reader.readLine()) != null) {
            LOGGER.info(out + "\n");
        }
        connection.disconnect();
    }

    public void findById(Integer id) throws IOException {
        LOGGER.info("Find by id news user RESTfull");
        url = new URL("http://localhost:9999/SerhiiBubniiService/news/by-query?id=" + id);
        LOGGER.info("Open connection");
        connection = (HttpURLConnection) url.openConnection();
        LOGGER.info("Set request parameters");
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "MediaType.APPLICATION_JSON");
        LOGGER.info("Check if the response status code is OK, Response Code -- " + connection.getResponseCode());
        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connection.getResponseCode());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String out;
        LOGGER.info("Output from Server .... \n");
        while ((out = reader.readLine()) != null) {
            LOGGER.info(out);
        }
        connection.disconnect();
    }

    public void create(String title, String description, String bodyNews, String linkToNews, String category) throws IOException {
        LOGGER.info("create news user RESTfull");
        url = new URL("http://localhost:9999/SerhiiBubniiService/news/add");
        LOGGER.info("Open connection");
        connection = (HttpURLConnection) url.openConnection();
        LOGGER.info("Set request parameters");
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");

        String input = "{ \"title\" : \"" + title + "\"," +
                "\"description\" : \"" + description + "\"," +
                "\"bodyNews\" : \"" + bodyNews + "\"," +
                "\"linkToNews\" : \"" + linkToNews + "\"," +
                "\"category\" : \"" + category + "\"" + "}";

        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(input.getBytes());
        outputStream.flush();
        LOGGER.info("Check if the response status code is OK, Response Code -- " + connection.getResponseCode());
        if (connection.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connection.getResponseCode());
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String output;
        LOGGER.info("Output from Server .... \n");
        while ((output = reader.readLine()) != null) {
            LOGGER.info(output);
        }
        connection.disconnect();
    }

    public void update(Integer id, String title, String description, String bodyNews, String linkToNews, String category) throws IOException {
        LOGGER.info("update news user RESTfull");
        url = new URL("http://localhost:9999/SerhiiBubniiService/news/update");
        LOGGER.info("Open connection");
        connection = (HttpURLConnection) url.openConnection();
        LOGGER.info("Set request parameters");
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");
        connection.setRequestProperty("Content-Type", "application/json");

        String input = "{ \"id\" : " + id + "," +
                "\"title\" : \"" + title + "\"," +
                "\"description\" : \"" + description + "\"," +
                "\"bodyNews\" : \"" + bodyNews + "\"," +
                "\"linkToNews\" : \"" + linkToNews + "\"," +
                "\"category\" : \"" + category + "\"" + "}";

        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(input.getBytes());
        outputStream.flush();

        LOGGER.info("Check if the response status code is OK, Response Code -- " + connection.getResponseCode());
        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connection.getResponseCode());
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String output;
        LOGGER.info("Output from Server .... \n");
        while ((output = reader.readLine()) != null) {
            LOGGER.info(output);
        }
        connection.disconnect();
    }
}
