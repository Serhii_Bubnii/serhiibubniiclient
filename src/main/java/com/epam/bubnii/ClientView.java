package com.epam.bubnii;

import com.epam.bubnii.interfaces.Printable;
import com.epam.bubnii.service.rest.RestClient;
import com.epam.bubnii.service.soap.Category;
import com.epam.bubnii.service.soap.News;
import com.epam.bubnii.service.soap.NewsServiceSoap;
import com.epam.bubnii.service.soap.NewsServiceSoapImplService;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ClientView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private NewsServiceSoap soapClient;
    private RestClient restClient;
    public static Logger LOGGER = Logger.getLogger(ClientView.class);

    public ClientView() {
        soapClient = new NewsServiceSoapImplService().getNewsServiceSoapImplPort();
        restClient = new RestClient();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show all news");
        menu.put("2", "\t2 - Show news by id");
        menu.put("3", "\t3 - Show news by category");
        menu.put("4", "\t4 - Create news");
        menu.put("5", "\t5 - Update news");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getAllNews);
        methodsMenu.put("2", this::getNewsById);
        methodsMenu.put("3", this::getAllNewsByCategory);
        methodsMenu.put("4", this::createNews);
        methodsMenu.put("5", this::updateNews);
    }

    private void getAllNews() {
        LOGGER.info("What service do you want use REST or SOAP?");
        LOGGER.info("Enter REST or SOAP...");
        String service = input.next().toUpperCase();
        if (service.equals("REST")) {
            try {
                restClient.findAll();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (service.equals("SOAP")) {
            LOGGER.info(soapClient.findAll());
        }
    }

    private void getNewsById() {
        LOGGER.info("What service do you want use REST or SOAP?");
        LOGGER.info("Enter REST or SOAP...");
        String service = input.next().toUpperCase();
        LOGGER.info("Enter id news...");
        Integer id = input.nextInt();
        if (service.equals("REST")) {
            try {
                restClient.findById(id);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (service.equals("SOAP")) {
            LOGGER.info(soapClient.findById(id));
        }
    }

    private void getAllNewsByCategory() {
        LOGGER.info("What service do you want use REST or SOAP?");
        LOGGER.info("Enter REST or SOAP...");
        String service = input.next().toUpperCase();
        LOGGER.info("Enter category news...");
        String category = input.next();
        if (service.equals("REST")) {
            try {
                restClient.findAllByCategory(category);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (service.equals("SOAP")) {
            LOGGER.info(soapClient.findAllByCategory(Category.valueOf(category)));
        }
    }

    private void createNews() {
        LOGGER.info("what service do you want use REST or SOAP?");
        LOGGER.info("Enter REST or SOAP...");
        String service = input.next().toUpperCase();
        News news = new News();
        LOGGER.info("Enter title news...");
        String title = input.nextLine();
        news.setTitle(title);
        LOGGER.info("Enter description news...");
        String description = input.nextLine();
        news.setDescription(description);
        LOGGER.info("Enter body news...");
        String bodyNews = input.nextLine();
        news.setBodyNews(bodyNews);
        LOGGER.info("Enter link to news...");
        String linkToNews = input.nextLine();
        news.setLinkToNews(linkToNews);
        LOGGER.info("Enter category news...");
        String category = input.next();
        news.setCategory(Category.valueOf(category));
        if (service.equals("REST")) {
            try {
                restClient.create(title, description, bodyNews, linkToNews, category);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (service.equals("SOAP")) {
            LOGGER.info(soapClient.create(news));
        }
    }

    private void updateNews() {
        LOGGER.info("what service do you want use REST or SOAP?");
        LOGGER.info("Enter REST or SOAP...");
        String service = input.next().toUpperCase();
        News news = new News();
        LOGGER.info("Enter id news...");
        Integer id = input.nextInt();
        news.setId(id);
        LOGGER.info("Enter title news...");
        String title = input.nextLine();
        news.setTitle(title);
        LOGGER.info("Enter description news...");
        String description = input.nextLine();
        news.setDescription(description);
        LOGGER.info("Enter body news...");
        String bodyNews = input.nextLine();
        news.setBodyNews(bodyNews);
        LOGGER.info("Enter link to news...");
        String linkToNews = input.nextLine();
        news.setLinkToNews(linkToNews);
        LOGGER.info("Enter category news...");
        String category = input.next();
        news.setCategory(Category.valueOf(category));
        if (service.equals("REST")) {
            try {
                restClient.update(id, title, description, bodyNews, linkToNews, category);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (service.equals("SOAP")) {
            soapClient.update(news);
        }
    }

    public void show() {
        String keyMenu;
        do {
            LOGGER.info("------------------------------------------------------------------------------------------");
            outputMenu();
            LOGGER.info("Please, select menu point.");
            keyMenu = input.next().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        LOGGER.info("MENU:");
        for (String str : menu.values()) {
            LOGGER.info(str);
        }
    }

}
