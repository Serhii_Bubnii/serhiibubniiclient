package com.epam.bubnii.interfaces;

@FunctionalInterface
public interface Printable {
    void print();
}
