package com.epam.bubnii.utills;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.apache.log4j.Logger;

import java.util.ArrayList;

import static org.testng.AssertJUnit.assertEquals;


public class HelperMethods {

    public static Logger LOGGER = Logger.getLogger(HelperMethods.class);

    public static void checkStatusIs200(Response res) {
        assertEquals("Status Check Failed!", 200, res.getStatusCode());
    }

    public static ArrayList getNewsCategoryList(JsonPath jp) {
        ArrayList videoIdList = jp.get("category");
        LOGGER.info(videoIdList.toString());
        return videoIdList;
    }

    public static ArrayList getNewsById(JsonPath jp) {
        ArrayList newsById = jp.get("id");
        LOGGER.info(newsById.toString());
        return newsById;
    }

    public static ArrayList mergeLists(ArrayList videoList, ArrayList relatedVideoList) {
        ArrayList mergedVideoList = new ArrayList(videoList);
        mergedVideoList.addAll(relatedVideoList);
        return mergedVideoList;
    }
}
