package com.epam.bubnii.utills;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.apache.log4j.Logger;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

public class RestUtil {

    public static Logger LOGGER = Logger.getLogger(RestUtil.class);

    public static String path;

    public static void setBaseURL(String baseURL) {
        LOGGER.info("Set base URL = " + baseURL);
        RestAssured.baseURI = baseURL;
    }

    public static void setBasePort(Integer port) {
        LOGGER.info("Set base port = " + port);
        RestAssured.port = port;
    }

    public static void setBasePath(String basePathTerm) {
        LOGGER.info("Set base path = " + basePathTerm);
        RestAssured.basePath = basePathTerm;
    }

    public static void resetBaseURL() {
        LOGGER.info("Reset base URL");
        RestAssured.baseURI = null;
    }

    public static void resetBasePort() {
        LOGGER.info("Reset base port");
        RestAssured.port = -1;
    }

    public static void resetBasePath() {
        LOGGER.info("Reset base path");
        RestAssured.basePath = null;
    }

    public static void setContentType(ContentType type) {
        LOGGER.info("Set content type = " + type.toString());
        given().contentType(type);
    }

    public static void  createSearchQueryPath(String searchTerm, String jsonPathTerm, String param, String paramValue) {
        LOGGER.info("Query path -- " + searchTerm + "/" + jsonPathTerm + "?" + param + "=" + paramValue);
        path = searchTerm + "/" + jsonPathTerm + "?" + param + "=" + paramValue;
    }

    public static void  createSearchPathParam(String param, String paramValue) {
        LOGGER.info("Param path -- " + param + "/" + paramValue + "/");
        path = param + "/" + paramValue + "/";
    }

    public static Response getResponse() {
        LOGGER.info("Get response");
        return get(path);
    }

    public static JsonPath getJsonPath(Response response) {
        String json = response.asString();
        LOGGER.info("Get JSON path -- " + json);
        return new JsonPath(json);
    }
}
